package com.example.pokemonfinder.Utils.Dao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class PokemonItem {
  @SerializedName("abilities")
  private ArrayList<String> mAbilities;
  @SerializedName("collectibles_slug")
  private String mCollectiblesSlug;
  @SerializedName("detailPageURL")
  private String mDetailPageURL;
  @SerializedName("featured")
  private String mFeatured;
  @SerializedName("height")
  private Double mHeight;
  @SerializedName("id")
  private Long mId;
  @SerializedName("name")
  private String mName;
  @SerializedName("number")
  private String mNumber;
  @SerializedName("slug")
  private String mSlug;
  @SerializedName("thumbnailAltText")
  private String mThumbnailAltText;
  @SerializedName("thumbnailImage")
  private String mThumbnailImage;
  @SerializedName("type")
  private ArrayList<String> mType;
  @SerializedName("weakness")
  private ArrayList<String> mWeakness;
  @SerializedName("weight")
  private Double mWeight;

  public ArrayList<String> getAbilities() {
    return mAbilities;
  }

  public String getCollectiblesSlug() {
    return mCollectiblesSlug;
  }

  public String getDetailPageURL() {
    return mDetailPageURL;
  }

  public String getFeatured() {
    return mFeatured;
  }

  public Double getHeight() {
    return mHeight;
  }

  public Long getId() {
    return mId;
  }

  public String getName() {
    return mName;
  }

  public String getNumber() {
    return mNumber;
  }

  public String getSlug() {
    return mSlug;
  }

  public String getThumbnailAltText() {
    return mThumbnailAltText;
  }

  public String getThumbnailImage() {
    return mThumbnailImage;
  }

  public ArrayList<String> getType() {
    return mType;
  }

  public ArrayList<String> getWeakness() {
    return mWeakness;
  }

  public Double getWeight() {
    return mWeight;
  }
}
