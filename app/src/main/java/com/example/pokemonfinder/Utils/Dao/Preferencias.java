package com.example.pokemonfinder.Utils.Dao;

public class Preferencias {

    private String name;
    private String type;
    private Integer type_position;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getType_position() {
        return type_position;
    }

    public void setType_position(Integer type_position) {
        this.type_position = type_position;
    }
}
