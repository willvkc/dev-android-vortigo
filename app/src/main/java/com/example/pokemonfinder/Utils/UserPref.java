package com.example.pokemonfinder.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.example.pokemonfinder.Utils.Dao.Preferencias;
import com.google.gson.Gson;

public class UserPref {

    private final Context context;
    public UserPref(Context context){
        this.context = context;
    }

    private static String shared = "usuario";

    public void setDados(@NonNull Preferencias model) {
        SharedPreferences settings = context.getSharedPreferences(shared, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("dados", new Gson().toJson(model));
        editor.commit();
    }

    public Preferencias getDados() {
        Preferencias resposta;
        Gson gson1 = new Gson();
        SharedPreferences settings = context.getSharedPreferences(shared, 0);
        String dados_user = settings.getString("dados", "");
        if (dados_user.isEmpty()) {
            return null;
        } else {
            resposta = gson1.fromJson(dados_user, Preferencias.class);
        }
        return resposta;
    }

    public void resetDados() {
        SharedPreferences settings = context.getSharedPreferences(shared, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString("dados", null);
        editor.commit();
    }




}
