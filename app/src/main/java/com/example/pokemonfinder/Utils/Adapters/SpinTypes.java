package com.example.pokemonfinder.Utils.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pokemonfinder.R;
import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Utilidade;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class SpinTypes extends ArrayAdapter<PokemonItem> {

    private final Context context;
    private final Utilidade utilidade;
    private final ArrayList<PokemonItem> values;

    public SpinTypes(Context context, int textViewResourceId, ArrayList<PokemonItem> values) {
        super(context, textViewResourceId, values);
        this.values = values;
        this.context = context;
        this.utilidade = new Utilidade(context);

    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public PokemonItem getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        TextView label = (TextView) super.getView(position, convertView, parent);
        label.setText(utilidade.tratarNome(values.get(position).getName()));

        return label;
    }

    @Override
    public View getDropDownView(final int position, View convertView, @NonNull ViewGroup parent) {
        View label = LayoutInflater.from(context).inflate(R.layout.spin_drop, parent, false);

        ImageView thumbnail = label.findViewById(R.id.thumbnail);
        TextView text1 = label.findViewById(R.id.text1);
        text1.setText(utilidade.tratarNome(values.get(position).getName()));

        Picasso.get().load(values.get(position).getThumbnailImage()).into(thumbnail);


        return label;
    }
}
