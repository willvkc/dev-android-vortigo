package com.example.pokemonfinder.Utils.Dao;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResultType {

    @SerializedName("results")
    private ArrayList<PokemonItem> results;

    public ArrayList<PokemonItem> getResults() {
        return results;
    }

    public void setResults(ArrayList<PokemonItem> results) {
        this.results = results;
    }
}
