package com.example.pokemonfinder.Utils.Adapters;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.pokemonfinder.R;
import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Utilidade;
import com.squareup.picasso.Picasso;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by willv on 26/10/2017.
 */

public class AdapPokemon extends RecyclerView.Adapter<viewAdapPokemon> {

    private AlertDialog alertDialog;
    private List<PokemonItem> pokemonItems;
    private Utilidade utilidade;
    private Context context;

    public AdapPokemon(Context context, List<PokemonItem> pokemonItems) {
        this.context = context;
        this.pokemonItems = pokemonItems;
        this.utilidade = new Utilidade(context);
    }

    @Override
    public viewAdapPokemon onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_pokemon, parent, false);
        viewAdapPokemon viewHolder = new viewAdapPokemon(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewAdapPokemon viewHolder, final int position) {
        final PokemonItem resultsItem = pokemonItems.get(position);
        viewHolder.name.setText(utilidade.tratarNome(resultsItem.getName()));
        Picasso.get().load(resultsItem.getThumbnailImage()).into(viewHolder.thumbnail);

        viewHolder.principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPokemon(resultsItem);
            }
        });

    }

    @Override
    public int getItemCount() {
        return pokemonItems.size();
    }

    private RecyclerView weakness, abilities, types;
    private TextView name, height, weight;
    private AlertDialog.Builder builder;
    private ImageView thumbnail;
    private Button btnClose;
    private View dialogLayout;

    private void openPokemon(PokemonItem resultsItem) {
        builder = new AlertDialog.Builder(context);
        if (alertDialog == null) {
            alertDialog = builder.create();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            dialogLayout = inflater.inflate(R.layout.dialog_pokemon, null);
            alertDialog.setView(dialogLayout);
            Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            name = dialogLayout.findViewById(R.id.name);
            btnClose = dialogLayout.findViewById(R.id.btnClose);
            thumbnail = dialogLayout.findViewById(R.id.thumbnail);
            weight = dialogLayout.findViewById(R.id.weight);
            height = dialogLayout.findViewById(R.id.height);
            weakness = dialogLayout.findViewById(R.id.weakness);
            types = dialogLayout.findViewById(R.id.types);
            abilities = dialogLayout.findViewById(R.id.abilities);
        }

        if (utilidade.isOpen(alertDialog)) return;

        Picasso.get().load(resultsItem.getThumbnailImage()).into(thumbnail);
        name.setText(resultsItem.getName());
        height.setText("Height: " + resultsItem.getHeight().toString());
        weight.setText("Weight: " + resultsItem.getWeight().toString());

        AdapString AdapWeakness = new AdapString(context, resultsItem.getWeakness());
        AdapString AdapTypes = new AdapString(context, resultsItem.getType());
        AdapString Adapabilities = new AdapString(context, resultsItem.getAbilities());

        weakness.setLayoutManager(new LinearLayoutManager(context));
        weakness.setItemAnimator(new DefaultItemAnimator());
        abilities.setLayoutManager(new LinearLayoutManager(context));
        abilities.setItemAnimator(new DefaultItemAnimator());
        types.setLayoutManager(new LinearLayoutManager(context));
        types.setItemAnimator(new DefaultItemAnimator());

        weakness.setAdapter(AdapWeakness);
        abilities.setAdapter(Adapabilities);
        types.setAdapter(AdapTypes);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utilidade.fecharDialog(alertDialog);
            }
        });
        utilidade.abrirDialog(alertDialog);


    }


}

class viewAdapPokemon extends RecyclerView.ViewHolder {

    @BindView(R.id.principal)
    RelativeLayout principal;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.thumbnail)
    ImageView thumbnail;

    viewAdapPokemon(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

