package com.example.pokemonfinder.Utils.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.pokemonfinder.R;
import com.example.pokemonfinder.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by willv on 26/10/2017.
 */

public class AdapString extends RecyclerView.Adapter<viewAdapString> {

    private ArrayList<String> resultsItemList;
    private Utilidade utilidade;
    private Context context;

    public AdapString(Context context, ArrayList<String> resultsItemList) {
        this.context = context;
        this.resultsItemList = resultsItemList;
        this.utilidade = new Utilidade(context);
    }

    @Override
    public viewAdapString onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_string, parent, false);
        viewAdapString viewHolder = new viewAdapString(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewAdapString viewHolder, final int position) {
        viewHolder.name.setText(utilidade.tratarNome(resultsItemList.get(position)));
    }

    @Override
    public int getItemCount() {
        return resultsItemList.size();
    }
}

class viewAdapString extends ViewHolder {

    @BindView(R.id.name)
    TextView name;

    viewAdapString(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

