package com.example.pokemonfinder.Utils.Endpoints;

import android.content.Context;

import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Dao.ResultType;
import com.example.pokemonfinder.Utils.Utilidade;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



public class PokemonsWS {

    private final Context context;
    private Utilidade utilidade;
    private PokemonsCall pokemonsCall;

    public PokemonsWS(Context context, PokemonsCall pokemonsCall) {
        this.context = context;
        this.utilidade = new Utilidade(context);
        this.pokemonsCall = pokemonsCall;
    }

    
    public void tipos(){
        utilidade.abrirDialog();
        Webservice service = utilidade.getRetrofit(false).create(Webservice.class);
        Call<ResultType> param = service.tipos();
        param.enqueue(new Callback<ResultType>() {
            @Override
            public void onResponse(Call<ResultType> call, final Response<ResultType> response) {
                if (response.isSuccessful()){
                    pokemonsCall.getResult(response.body().getResults(), 0);
                }else {
                    utilidade.mostrarToast("Error");
                }

            }

            @Override
            public void onFailure(Call<ResultType> call, Throwable t) {
                utilidade.fecharDialog();
                utilidade.mostrarToast("Error: " + t.getMessage());
            }
        });
    }


    public void lista(Boolean open_dialog){
        if (open_dialog) utilidade.abrirDialog();
        Webservice service = utilidade.getRetrofit(false).create(Webservice.class);
        Call<ArrayList<PokemonItem>> param = service.lista();
        param.enqueue(new Callback<ArrayList<PokemonItem>>() {
            @Override
            public void onResponse(Call<ArrayList<PokemonItem>> call, final Response<ArrayList<PokemonItem>> response) {
                utilidade.fecharDialog();
                if (response.isSuccessful()){
                    pokemonsCall.getResult(response.body(), 1);
                }else {
                    utilidade.mostrarToast("Error");
                }

            }

            @Override
            public void onFailure(Call<ArrayList<PokemonItem>> call, Throwable t) {
                utilidade.fecharDialog();
                utilidade.mostrarToast("Error: " + t.getMessage());
            }
        });
    }


    public interface PokemonsCall {
        void getResult(ArrayList<PokemonItem> resultado, int categoria);

    }


}
