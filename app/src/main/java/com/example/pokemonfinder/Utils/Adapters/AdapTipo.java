package com.example.pokemonfinder.Utils.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pokemonfinder.R;
import com.example.pokemonfinder.Telas.Pesquisa;
import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Utilidade;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by willv on 26/10/2017.
 */

public class AdapTipo extends RecyclerView.Adapter<viewAdapTipo> {

    private List<PokemonItem> resultsItemList;
    private Utilidade utilidade;
    private Context context;

    public AdapTipo(Context context, List<PokemonItem> resultsItemList) {
        this.context = context;
        this.resultsItemList = resultsItemList;
        this.utilidade = new Utilidade(context);
    }

    @Override
    public viewAdapTipo onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_tipo, parent, false);
        viewAdapTipo viewHolder = new viewAdapTipo(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewAdapTipo viewHolder, final int position) {
        final PokemonItem resultsItem = resultsItemList.get(position);
        viewHolder.name.setText(utilidade.tratarNome(resultsItem.getName()));
        Picasso.get().load(resultsItem.getThumbnailImage()).into(viewHolder.thumbnail);
        viewHolder.principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, Pesquisa.class);
                intent.putExtra("categoria", 0);
                intent.putExtra("type", resultsItem.getName());
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return resultsItemList.size();
    }
}

class viewAdapTipo extends ViewHolder {

    @BindView(R.id.principal)
    LinearLayout principal;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.thumbnail)
    ImageView thumbnail;

    viewAdapTipo(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

