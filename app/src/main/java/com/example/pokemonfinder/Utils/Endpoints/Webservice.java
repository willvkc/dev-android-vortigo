package com.example.pokemonfinder.Utils.Endpoints;


import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Dao.ResultType;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Webservice {

    String URL = "https://vortigo.blob.core.windows.net/files/pokemon/data/";

    @GET("types.json")
    Call<ResultType> tipos();

    @GET("pokemons.json")
    Call<ArrayList<PokemonItem>> lista();



}
