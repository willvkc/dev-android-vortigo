package com.example.pokemonfinder.Telas;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.pokemonfinder.R;
import com.example.pokemonfinder.Utils.Adapters.AdapPokemon;
import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Endpoints.PokemonsWS;
import com.example.pokemonfinder.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Pesquisa extends AppCompatActivity implements PokemonsWS.PokemonsCall {

    @BindView(R.id.lista)
    RecyclerView lista;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.mensagem)
    TextView mensagem;
    @BindView(R.id.linMnsg)
    LinearLayout linMnsg;

    private int categoria;
    private Bundle extras;
    private Context context;
    private String type, name;
    private MenuItem menuItem;
    private Utilidade utilidade;
    private PokemonsWS pokemonsWS;
    private SearchView mSearchView;
    private AdapPokemon adapPokemon;
    private ArrayList<PokemonItem> arrayPokemons;
    private ArrayList<PokemonItem> arrayListPesquisa = new ArrayList<>();
    private ArrayList<PokemonItem> arrayListPesquisaName = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rc_list);
        ButterKnife.bind(this);
        context = this;
        extras = getIntent().getExtras();
        utilidade = new Utilidade(context);
        pokemonsWS = new PokemonsWS(this, this);

        ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
        TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        txtTitle.setText("Pokémon Finder");
        abar.setCustomView(viewActionBar, params);

        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);

        if (extras != null) {
            categoria = extras.getInt("categoria");

            switch (categoria) {
                case 0:
                    type = extras.getString("type");
                    break;

                case 1:
                    name = extras.getString("name");
                    break;
            }
            pokemonsWS.lista(true);

        }

    }

    @Override
    public void getResult(ArrayList<PokemonItem> resultado, int suit) {
        switch (suit) {

            case 1:
                arrayPokemons = resultado;
                listar();

                break;
        }
    }

    private void listar() {
        arrayListPesquisa.clear();

        switch (categoria) {
            case 0:
                for (PokemonItem item : arrayPokemons) {

                    for (String typeI : item.getType()) {
                        if (typeI.toLowerCase().equals(type.toLowerCase())) {
                            arrayListPesquisa.add(item);
                        }
                    }
                }

                break;

            case 1:
                arrayListPesquisa.addAll(arrayPokemons);
                MenuItemCompat.expandActionView(menuItem);
                mSearchView.clearFocus();
                mSearchView.setQuery(name, true);

                break;
        }

        new Handler().post(new Runnable() {
            @Override
            public void run() {

                mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        listar(s);
                        return true;
                    }
                });

            }
        });

        if (utilidade.tratarLista(linMnsg, lista, arrayListPesquisa)) {
            adapPokemon = new AdapPokemon(context, arrayListPesquisa);
            lista.setLayoutManager(new LinearLayoutManager(context));
            lista.setItemAnimator(new DefaultItemAnimator());
            lista.setAdapter(adapPokemon);
            listar(name);
        }

    }

    private void listar(String busca) {
        if (busca == null) return;
        arrayListPesquisaName.clear();

        for (PokemonItem item : arrayListPesquisa) {

            if (item.getName().toLowerCase().contains(busca.toLowerCase())) {
                arrayListPesquisaName.add(item);
            }
        }

        if (utilidade.tratarLista(linMnsg, lista, arrayListPesquisaName)) {
            adapPokemon = new AdapPokemon(context, arrayListPesquisaName);
            lista.setLayoutManager(new LinearLayoutManager(context));
            lista.setItemAnimator(new DefaultItemAnimator());
            lista.setAdapter(adapPokemon);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        menuItem = menu.findItem(R.id.search);
        mSearchView = (SearchView) menuItem.getActionView();
        mSearchView.setQueryHint("Search Pokémon...");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }


}
