package com.example.pokemonfinder.Telas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.pokemonfinder.R;
import com.example.pokemonfinder.Utils.Adapters.AdapPokemon;
import com.example.pokemonfinder.Utils.Adapters.AdapTipo;
import com.example.pokemonfinder.Utils.Adapters.SpinTypes;
import com.example.pokemonfinder.Utils.Dao.PokemonItem;
import com.example.pokemonfinder.Utils.Dao.Preferencias;
import com.example.pokemonfinder.Utils.Endpoints.PokemonsWS;
import com.example.pokemonfinder.Utils.UserPref;
import com.example.pokemonfinder.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Home extends AppCompatActivity implements PokemonsWS.PokemonsCall {

    @BindView(R.id.rcTypes)
    RecyclerView rcTypes;
    @BindView(R.id.rcPokemons)
    RecyclerView rcPokemons;

    private Spinner tipos;
    private Context context;
    private AdapTipo adapTipo;
    private UserPref userPref;
    private Button btnConcluir;
    private Utilidade utilidade;
    private SpinTypes dataAdapter;
    private PokemonsWS pokemonsWS;
    private SearchView mSearchView;
    private TextInputEditText nome;
    private AlertDialog dialogPref;
    private AdapPokemon adapPokemon;
    private AlertDialog.Builder builder;
    private ArrayList<PokemonItem> arrayTipos, arrayPokemons;
    private ArrayList<PokemonItem> arrayListPesquisa = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content);
        ButterKnife.bind(this);
        context = this;
        userPref = new UserPref(context);
        utilidade = new Utilidade(context);
        pokemonsWS = new PokemonsWS(context, this);
        pokemonsWS.tipos();

        ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
        TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        txtTitle.setText("Pokémon Finder");
        abar.setCustomView(viewActionBar, params);

        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeAsUpIndicator(R.drawable.ic_settings);
        abar.setHomeButtonEnabled(true);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);

        mSearchView = (SearchView) menu.findItem(R.id.search).getActionView();
        //Define um texto de ajuda:
        mSearchView.setQueryHint("Search Pokémon...");

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (!s.isEmpty()) {
                    Intent intent = new Intent(context, Pesquisa.class);
                    intent.putExtra("categoria", 1);
                    intent.putExtra("name", s);
                    startActivity(intent);
                    return true;
                } else {
                    return false;
                }

            }

            @Override
            public boolean onQueryTextChange(String s) {

                return false;
            }
        });


        switch (item.getItemId()) {

            case android.R.id.home:
                abrirPref();
                break;

        }

        return super.onOptionsItemSelected(item);
    }


    private void abrirPref() {

        if (builder == null) {
            builder = new AlertDialog.Builder(context);
            dialogPref = builder.create();
            dialogPref.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_up;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogLayout = inflater.inflate(R.layout.dialog_pref, null);
            dialogPref.setView(dialogLayout);
            dialogPref.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogPref.requestWindowFeature(Window.FEATURE_NO_TITLE);
            nome = dialogLayout.findViewById(R.id.nome);
            tipos = dialogLayout.findViewById(R.id.tipos);
            btnConcluir = dialogLayout.findViewById(R.id.btnConcluir);
            dataAdapter = new SpinTypes(context, R.layout.spin_item, arrayTipos);
            tipos.setAdapter(dataAdapter);

        }
        utilidade.abrirDialog(dialogPref);

        if (userPref.getDados() != null) {
            nome.setText(userPref.getDados().getName());
            tipos.setSelection(userPref.getDados().getType_position());
        }

        btnConcluir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nome.getText().toString().isEmpty()) {
                    utilidade.mostrarToast("Nome deve ser preenchido.");
                    return;
                } else {
                    Preferencias preferencias = new Preferencias();
                    preferencias.setName(nome.getText().toString());
                    preferencias.setType(arrayTipos.get(tipos.getSelectedItemPosition()).getName());
                    preferencias.setType_position(tipos.getSelectedItemPosition());
                    userPref.setDados(preferencias);
                    utilidade.fecharDialog(dialogPref);
                    listar();
                }

            }
        });


    }

    @Override
    public void getResult(ArrayList<PokemonItem> resultado, int categoria) {
        switch (categoria) {
            case 0:
                arrayTipos = resultado;
                listarTipos();
                pokemonsWS.lista(false);
                break;

            case 1:
                arrayPokemons = resultado;
                listar();


                break;
        }
    }


    private void listarTipos() {
        adapTipo = new AdapTipo(context, arrayTipos);
        rcTypes.setLayoutManager(new GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false));
        rcTypes.setItemAnimator(new DefaultItemAnimator());
        rcTypes.setAdapter(adapTipo);

    }


    private void listar() {
        arrayListPesquisa.clear();

        if (userPref.getDados() == null) {
            abrirPref();
        } else {
            for (PokemonItem item : arrayPokemons) {
                for (String type : item.getType()) {
                    if (type.toLowerCase().equals(userPref.getDados().getType().toLowerCase())) {
                        arrayListPesquisa.add(item);
                    }
                }
            }

        }

        adapPokemon = new AdapPokemon(context, arrayListPesquisa);
        rcPokemons.setLayoutManager(new LinearLayoutManager(context));
        rcPokemons.setItemAnimator(new DefaultItemAnimator());
        rcPokemons.setAdapter(adapPokemon);

    }

}
